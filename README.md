# nPulse

## Brief

Very fast pulse generator for TDR.

## Spec

* **Freq**: xxx to yyy Hz
* **Edge time**: xx ns
* **Zout**: xx to yy Ω 

![TOP](npulse_top.png) ![BOT](npulse_bot.png)